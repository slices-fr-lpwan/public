# Slices FR :: LPWAN Services

![Slices FR Icon](media/slices-fr-icon.png)

La mission de SLICES-FR est de contribuer à la compétitivité dans les systèmes et technologies de communication avancés qui sont au cœur de la plupart des domaines stratégiques de nos sociétés. Il couvre une diversité de systèmes et réseaux de communication allant de l'Internet des objets au Cloud Computing. Cela englobe tous les défis scientifiques de l'informatique distribuée (centres de données, informatique dans les nuages, edge et FOG computing) et des réseaux (IoT, Internet, réseaux sans fil et réseaux cellulaires (5G/6G/etc)).

SLICES-FR déploie et exploite une infrastructure à grande échelle permettant l'accès contrôlé et uniforme à des technologies de pointe hétérogènes et variées. Il répondra à la nécessité de soutenir la recherche fondamentale dans ces domaines où l'accès à de tels instruments est essentiel. En outre, il a pour ambition de créer des synergies entre les acteurs universitaires, industriels et commerciaux afin d'accélérer l'accès au marché des technologies TIC de base, mais aussi dans les secteurs verticaux (secteurs d'application). 

Le service LPWAN de SLICES_FR permet l’expérimentation, le prototypage et la mesure sur toute la chaîne de la donnée de sa capture dans l’IoT ou les réseaux cellulaires depuis son stockage et sa restitution en passant par son transport via des réseaux filaires et sans fil de différentes technologies et son traitement à différents niveaux que ce soit sur les objets terminaux, en périphérie de réseau (edge computing) ou dans le cloud.

## Links
* [Slices-RI: Scientific Large Scale Infrastructure for Computing/Communication Experimental Studies](https://www.slices-ri.eu/)
* [Slices FR](https://slices-fr.eu/)
